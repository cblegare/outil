# flake8: noqa
import functools
import sys
import time

examples = []


def example(fn):
    """Wrap the examples so they generate readable output"""

    @functools.wraps(fn)
    def wrapped():
        try:
            sys.stdout.write("Running: %s\n" % fn.__name__)
            fn()
            sys.stdout.write("\n")
        except KeyboardInterrupt:
            sys.stdout.write("\nSkipping example.\n\n")
            # Sleep a bit to make killing the script easier
            time.sleep(0.2)

    examples.append(wrapped)
    return wrapped


@example
def mine():
    import time

    import progressbar

    def gen():
        yield from range(80)

    all_steps = {"first": gen, "second": gen, "third": gen, "fourth": gen}

    for step_name, steps in all_steps.items():
        marker = progressbar.AnimatedMarker(
            markers=["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"],
            default="✓",
        )
        widgets = ["{!s:<10} ".format(step_name + ":"), marker]
        with progressbar.ProgressBar(widgets=widgets) as bar:
            for _ in bar(steps()):
                if step_name == "third":
                    marker.default = "✗"
                time.sleep(0.1)


@example
def custom_spinners():
    import logging
    import time

    import progressbar
    from spinners import Spinners

    progressbar.streams.wrap_stderr()
    logging.basicConfig()

    spinner = Spinners.arrow3.value["frames"]
    end = "Done!"

    widgets = [
        "  ",
        progressbar.AnimatedMarker(spinner, default=end),
    ]
    for i in progressbar.progressbar(range(20), widgets=widgets):
        time.sleep(0.1)
        # logging.error('Got %d', i)


@example
def yaspin():
    from yaspin import kbi_safe_yaspin as yaspin

    def reduce_yield_text(spinner, fn, *args, **kwargs):
        orig_text = spinner.text
        for yielded in fn(*args, **kwargs):
            spinner.text = "{!s} {!s}".format(orig_text, yielded)

    def spin(*yaspin_args, **yaspin_kwargs):
        def decorator(fn):
            text = yaspin_kwargs.pop("text", fn.__name__)

            @functools.wraps(fn)
            def wrapped(*args, **kwargs):
                with yaspin(*args, text=text, **kwargs) as spinner:
                    try:
                        result = reduce_yield_text(spinner, fn, *args, **kwargs)
                        spinner.write("✔ {!s} complete".format(text))
                        return result
                    except Exception as e:
                        spinner.write("✗ {!s} failed".format(text))
                        raise

            return wrapped

        return decorator

    @spin(text="Downloading")
    def download():
        for a in "abcd":
            yield a
            time.sleep(1)

    @spin()
    def error():
        yield "e"
        time.sleep(1)
        yield "f"
        time.sleep(1)
        raise Exception("oups")

    download()
    error()


def main(*tests):
    for example in examples:
        if not tests or example.__name__ in tests:
            example()
        else:
            print("Skipping", example.__name__)


if __name__ == "__main__":
    try:
        main(*sys.argv[1:])
    except KeyboardInterrupt:
        sys.stdout("\nQuitting examples.\n")

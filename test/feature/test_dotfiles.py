import textwrap
from pathlib import Path

import pytest

from outil.template import CautiousTemplate
from outil.template.cookiecutter import Cookiecutter
from outil.template.setuptools import (
    DOTFILES_ENTRYPOINT_GROUP,
    SetuptoolsEntrypointTemplateRepository,
)


def test_render_dotfiles_to_folder(template_repository, tmpdir):
    parent_folder = Path(tmpdir)
    dotfiles_destination = parent_folder.joinpath("dotfiles")

    template = template_repository["mydotfiles"]

    rendered = template.render(parent_folder, {})

    assert dotfiles_destination == rendered
    assert (
        textwrap.dedent(
            "# If not running interactively, don't do anything".strip("\n")
        )
        in rendered.joinpath("bashrc").read_text()
    )


@pytest.fixture()
def template_repository():
    def _template_from_path(path: Path):
        return CautiousTemplate(Cookiecutter(path))

    return SetuptoolsEntrypointTemplateRepository(
        DOTFILES_ENTRYPOINT_GROUP, _template_from_path
    )

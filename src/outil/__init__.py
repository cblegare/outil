"""
###########################
|outil| - The One UTIL tool
###########################

Attributes:
    __project__ (str): Runtime value for the package name and command
        line entrypoint.

    __version__ (str): Runtime value for the current version.

    __dist__ (:class:`pkg_resources.Distribution`): Runtime guess about
        its installation.  This may be a tissue of lies since it
        **will** be created, whether an installed distribution can be
        found or not.  This will happen if the package is not
        installed.

        .. warning:: Development (editable) installations may break
            other packages from the same implicit namespace. See
            https://github.com/pypa/packaging-problems/issues/12

        See :doc:`pkg_resources` for details.

    _PACKAGE (str): The advertised name of **this** package. We
        introspect the current installation for convenience.
"""
from pkg_resources import Distribution, get_distribution

_PACKAGE = "outil"


__dist__: Distribution = get_distribution(_PACKAGE)
__project__: str = __dist__.project_name
__version__: str = __dist__.version

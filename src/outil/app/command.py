"""
########
Commands
########

Commands are operations available to users.  Since |outil| does not hold
much data but expose a lots of side-effect or write operation instead,
we use a dedicated command model as covered by various authors under
the **Command Query Responsibility Segregation** umbrella.
"""

from __future__ import annotations

from typing import Any, Callable, List, NoReturn, Type, TypeVar

ReturnType = TypeVar("ReturnType")


class CommandHandler:
    """
    Locate dependencies of commands and invoke them.

    This class defines an interface for a service locator to be used
    by :class:`outil.app.command.Command` objects.
    """

    def inject(
        self, callback: Callable[..., ReturnType]
    ) -> Callable[[], ReturnType]:
        """
        Locate required services and returned a parametrized function.

        Args:
            callback: A function that takes services as parameters.

        Returns:
            The same function but with services already bound as arguments.
        """
        raise NotImplementedError


class Command:
    """
    Implement a user-accessible command with the application.

    A command is basically en inverted service.  Parameters are set in
    its constructor while dependent services are provided to its API.
    In  other words,

    *   the services it uses (for accessing a database, for instance)
        are defined as :meth:`outil.app.command.Command.execute`
        parameters,

    *   the values it uses are set using its constructor and

    *   the way values interact with services is written as the
        :meth:`outil.app.command.Command.execute` method body.

    It must obeys a few rules:

    **A command is a value object**

        Its constructor arguments must define its identity.  That means
        that two commands of the same class called with the same
        constructor arguments should be equivalent (also equal).
        In other words, the following must always be true:

        >>> arguments = dict(a=2, b="some string")
        >>> Command(**arguments) == Command(**arguments)
        True

        To express that explicitely, this class expose a
        :meth:`outil.app.command.Command.__repr__` method that
        returns valid python code to create and exact copy of the
        object.

        As a corollary, the following should also always be true:

        >>> import outil.app.command
        >>> c = Command(a=2, b="some string")
        >>> eval(repr(c)) == c
        True

        This rules makes sure a command and be **serialized** for later use.

    **Dependent services must be locatable**

        This class is expected to be used in conjunction with
        :meth:`outil.app.ioc.CommandHandler.execute`
        which leverages services defined in :mod:`outil.app.services`.

    .. hint:: Although the above code samples do work out-of-the-box,
        this class is intented to be subclassed and its constructor
        redefined.

    Attributes:
        __commands__: All subclasses of this class are stored in the
            order the interpreter found them,  This can be useful
            for testing purposes, for instance.
    """

    __commands__: List[Type[Command]] = []

    def __init__(self, **kwargs: Any):
        """
        A default constructor for commands, mostly for documentation purposes.

        Args:
            **kwargs: Only keyword arguments are processed and set to the
                ``__dict__``.
        """
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __init_subclass__(cls, **kwargs: Any):
        """
        Register subclasses in the ``__commands__`` class property.

        Args:
            **kwargs: As per PEP-487. these are keyword arguments passed to
                the class definition.  For instance

                .. code-block:: python
                    class MyCommand(Command, myarg="foo"): pass

                ``kwargs`` would be a dict ``{"myarg": "foo"}``.
        """
        cls.__commands__.append(cls)

    def __repr__(self) -> str:
        """
        Provide a interpreter-readable representation of self.

        Returns:
            Dotted fully qualified class name with parameters.
        """
        params = ", ".join(
            f"{key.lstrip('_')!s}={value!r}"
            for key, value in self.__dict__.items()
        )
        return (
            f"{self.__class__.__module__}"
            f".{self.__class__.__qualname__}({params!s})"
        )

    def __eq__(self, other: Any) -> bool:
        """
        Commands are value-objects, thus equality is defined by their ``__repr__``.

        Args:
            other: Right operand of the ``==`` operator.

        Returns:
            Whether or not both objects are equivalent.
        """
        return repr(self) == repr(other)

    def __hash__(self) -> int:
        """
        Commands can be indexed by their value.

        Returns:
            A fixed sized integer that identifies this particular command.
        """
        return hash(repr(self))

    def do(self, service_locator: CommandHandler) -> Any:
        """
        Implement this command actual behavior and using available services.

        Examples:

            Given the :meth:`outil.app.command.CommandHandler.inject`
            method above, one can implement the actual behavior within a
            inner function like so:

            .. code-block:: python

                def do(self, service_locator: CommandHandler) -> Any
                    def func(service_name, ServiceType) -> None:
                        # do something with service
                        pass
                    parametrized_with_service_instance = service_locator.inject(func)
                    return parametrized_with_service_instance()

            Or define the ``func`` function as a private instance method.

            This project uses a convention that actually breaks the
            Liskov Substitution Principle but greatly simplify the
            testing of parameters names and types. The below method
            :meth:`outil.app.command.Command._do` is the one passed to
            :meth:`outil.app.command.CommandHandler.inject` and subclasses
            must specify parameters having this in mind.

        Returns:
            Specific command may return some result.
        """
        return service_locator.inject(self._do)()

    def _do(self, *args: Any, **kwargs: Any) -> Any:
        raise NotImplementedError()

    def undo(self, service_locator: CommandHandler) -> NoReturn:
        """
        Reserved for future use.
        """
        raise NotImplementedError()

    @property
    def view(self) -> NoReturn:
        """
        Reserved for future use.
        """
        raise NotImplementedError()

"""
" A vim configuration
" ===================
"
" This vim configuration is one of a vim newbie.
"
" Inspiration was taken from
" - http://nerditya.com/code/guide-to-neovim/
" - https://realpython.com/vim-and-python-a-match-made-in-heaven/
"
" Conventions in this file
" ------------------------
"
" Commands expressed as <C-X>
"     means the CTRL key plus the X (letter) key.
"""

"""
" uber condition
" if we're awesome else we suck
if has('nvim')
    " Neovim specific commands
else
    " Standard vim specific commands
endif
"""

"""
" vim-plug section
" ================
"
" Update with
"     :PlugInstall
:"
" See https://github.com/junegunn/vim-plug
call plug#begin()
Plug 'tpope/vim-sensible'               " sensible defaults
Plug 'arcticicestudio/nord-vim'         " blueish colors
Plug 'vim-airline/vim-airline'          " rich status bar
Plug 'vim-airline/vim-airline-themes'   " nicer status bar
Plug 'scrooloose/nerdtree'              " file browser
Plug 'tpope/vim-fugitive'               " git wrapper
Plug 'airblade/vim-gitgutter'           " git diff in margin
Plug 'Xuyuanp/nerdtree-git-plugin'      " git status in nerdtree
Plug 'christoomey/vim-tmux-navigator'   " better integration with tmux
Plug 'ncm2/ncm2'                        " completion framework
Plug 'roxma/nvim-yarp'                  " dependency for ncm2
Plug 'davidhalter/jedi-vim'             " great python static analysis
Plug 'ncm2/ncm2-jedi'                   " jedi to ncm2 integration
Plug '907th/vim-auto-save'              " autosave
Plug 'gu-fan/riv.vim'                   " RestructuredText
call plug#end()
"
" Here are some plugins to consider
" - CtrlP is a fuzzy file finder
" - Airline is a status bar
"
" Plugin configuration
" --------------------
"
" vim-airline/vim-airline
let g:airline_theme='minimalist'
"
" 907th/vim-auto-save
let g:auto_save = 0
augroup ft_markdown
  au!
  au FileType markdown let b:auto_save = 1
augroup END
augroup ft_python
  au!
  au FileType python let b:auto_save = 1
augroup END
"
" scrooloose/nerdtree
" Open NERDTree by default
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" Nicer and simpler NERDTree
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
" Show hidden files
let NERDTreeShowHidden = 1
" Unclutter NERDTree
let NERDTreeIgnore = ['\.pyc$', '\.egg-info$', '__pycache__']
"
" airblade/vim-gitgutter
"   enabled by default
let g:gitgutter_enabled = 1
"""

"""
" Look and feel
" -------------
"
" Colors
colorscheme nord
"
" Mouse integration enabled in all modes
set mouse=a
"
" Use the sensible default clipboard registry, always.
"   Graphical applications usually use the CLIPBOARD registry.
"   The PRIMARY registry holds the last selection on terminal
"   applications. The SECONDARY registry exists for some reason.
"
"   I always forget about all that and struggle to copy and paste
"   accross Vim, Tmux, webbrowser, whatever,
"
"   This settings tells vim to always use the only clipboard
"   registry I always knew about.  This implies that all
"   yank, dank and paste commands will always use the same good old
"   clipboard as any graphical user interfaces,
set clipboard+=unnamedplus
"
" Nvim shows weird symbols (�[2 q) when changing modes
" https://github.com/neovim/neovim/wiki/FAQ#nvim-shows-weird-symbols-2-q-when-changing-modes
:set guicursor=
" Workaround some broken plugins which set guicursor indiscriminately.
:autocmd OptionSet guicursor noautocmd set guicursor=
"""

"""
" Navigation
"   from https://robots.thoughtbot.com/vim-splits-move-faster-and-more-naturally#easier-split-navigations
"
" More natural split opening
"   Open new split panes to right and bottom, which feels
"   more natural than Vim’s default:
set splitbelow
set splitright
"
" Easier change splits
"   Navigate between splits in one hotkey combination
"   instead of two.
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
"
" Resizing splits
"   Vim’s defaults are useful for changing split shapes:
"
"   Max out the height of the current split
"     ctrl + w _
"
"   Max out the width of the current split
"     ctrl + w |
"
"   Normalize all split sizes, which is very handy when resizing terminal
"     ctrl + w =
"
" More split manipulation
"
"   Swap top/bottom or left/right split
"     ctrl + W R
"
"   Break out current window into a new tabview
"     ctrl + w T
"
"   Close every window in the current tabview but the current one
"     ctrl + W o
"
"""

"""
" Map the leader key to SPACE
"
" WARNING: this may cause addiction and I will miss it on non-configured
" systems
" Leader key is like a command prefix. You can map actions to
" <Leader>key to easily do some complex stuff. For some reason
" most of the configurations map the comma key to leader. While
" that may work I decided to map the leader key to space. I don't
" really have a use for the spacebar in normal mode and, having
" the most prominent key on the keyboard for easy use with both
" hands makes a difference.
let mapleader="\<SPACE>"
"""

"""
" Show matching brakets
set showmatch
"
" Display line numbers
set number
" Absolute numbers in INSERT mode
autocmd InsertEnter * :set norelativenumber
" Relative numbers in NORMAL mode
autocmd InsertLeave * :set relativenumber 
"
" Continue comment marker on new lines
set formatoptions+=o
"
" Expand spaces when TAB is pressed
set expandtab
"
" Expand that many spaces when rendering TABs
set tabstop=4
"
" Indentation amount for < and > commands
set shiftwidth=4
"
" Tell Vim which characters to show for 
" expanded TABs, trailing whitespace, and end-of-lines.
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif
set list                " Show problematic characters.
"
" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/
"
" And anyways, we want to get rid of trailing whitespaces
" at least for some file types
autocmd BufWritePre *.py,*.yaml :%s/ \+$//ge
"
" Make searching case insentitive
set ignorecase
" ... unless the query has capital letters.
set smartcase
" The 'g' flag should be default sith :s/foo/bar/.
set gdefault
"
" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif
"
" Search and Replace is <Leader-S>
nmap <Leader>s :%s//g<Left><Left>
"
" Use ; for commands instead of :
nnoremap ; :
" Use Q to execute default register (sadly, I don't know what it means)
nnoremap Q @q
"""
"""
" Vim behavior
" ------------
"
" Automatically reload config
augroup myvimrc
    au!
    au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc,init.vim so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END
"""

"""
#####################
Templates as plugins!
#####################

Why? Why not?!
"""


from pathlib import Path
from types import ModuleType
from typing import Callable, Iterator, KeysView

import pkg_resources

from outil import __project__
from outil.template import Template, Templates

DOTFILES_ENTRYPOINT_GROUP = f"{__project__}.templates"


class SetuptoolsEntrypointTemplateRepository(Templates):
    def __init__(
        self, entrypoint_group: str, template_ctor: Callable[[Path], Template]
    ):
        """
        Collect templates from setuptools entrypoints.

        Here is an example configuration within this project's `setup.cfg`:

        .. literalinclude:: ../../setup.cfg
            :language: ini
            :lines: 57-62
            :emphasize-lines: 60-61

        .. tip::
            We provide one template as a built-in entrypoint
            (:mod:`outil.templates.dotfiles`)
            which can be used as a reference or exemple,

        Found templates are index with both their entrypoint name and their
        fully qualified name.
        In the example above, the **entrypoint name** is ``mydotfiles``
        and the **fully qualified name** is ``outil.templates.dotfiles``.

        .. warning::
            Collision between names is not prevented.

        Entrypoints can load into both modules or
        :class:`outil.template.Template` instances.

        When an entrypoints loads as a module, the module is considered to be
        a folder containing a template.  This folder will be loaded even when
        this is run within an `egg` or an other compressed build like ones
        made with `pyinstaller` since we use the `pkg_resource` API to extract
        files and folders into a suitable filesystem.

        Args:

            entrypoint_group: The entrypoint group to fetch from.
                Entrypoint groups are the keys used to define a bunch
                of entrypoints using `setup.py` or `setup.cfg`.

                The most widely known entrypoint group is ``console_script``.

            template_ctor: Since this allows for modules to be directly used
                as template folders, this function is used to provide the
                implementation from these folders.
        """
        self._wrapped_repo = {}

        for entrypoint in pkg_resources.iter_entry_points(entrypoint_group):
            loaded = entrypoint.load()

            if isinstance(loaded, Template):
                template = loaded
            elif isinstance(loaded, type) and issubclass(loaded, Template):
                template = loaded()
            elif isinstance(loaded, ModuleType):
                *package_parts, name = loaded.__name__.split(".")
                package = ".".join(package_parts)
                template_directory = pkg_resources.resource_filename(
                    package, name
                )
                template = template_ctor(Path(template_directory))
                template.__doc__ = loaded.__doc__
            else:
                raise RuntimeError(f"Invalid template: {loaded}")

            if entrypoint.attrs:
                fullname = (
                    f"{entrypoint.module_name}:{'.'.join(entrypoint.attrs)}"
                )
            else:
                fullname = entrypoint.module_name

            self._wrapped_repo[fullname] = template
            self._wrapped_repo[entrypoint.name] = template

    def __len__(self) -> int:
        return self._wrapped_repo.__len__()

    def __getitem__(self, key: str) -> Template:
        return self._wrapped_repo.__getitem__(key)

    def __repr__(self) -> str:
        return self._wrapped_repo.__repr__()

    def __iter__(self) -> Iterator[str]:
        return self._wrapped_repo.__iter__()

    def __contains__(self, key: object) -> bool:
        return self._wrapped_repo.__contains__(key)

    def keys(self) -> KeysView[str]:
        return self._wrapped_repo.keys()

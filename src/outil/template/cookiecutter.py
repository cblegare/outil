"""
#############################
Templates with Cookiecutter_.
#############################

Have file name templates, hooks, testing instrumentation
for pytest, Cookiecutter is a neat scaffolding tool. This
module provides the basics for using it for templating within
|outil|.

.. _Cookiecutter: https://github.com/cookiecutter/cookiecutter
"""

from __future__ import annotations

import shutil
import tempfile
from pathlib import Path
from typing import Dict, Optional, cast

from typing_extensions import Protocol

from outil.template import Template


class CookiecutterAPI(Protocol):
    """
    Describe the Cookiecutter invocation protocol.

    This class is solely used for type annotations.
    """

    def __call__(
        self,
        template: str,
        checkout: Optional[str] = None,
        no_input: bool = False,
        extra_context: Optional[Dict[str, str]] = None,
        replay: bool = False,
        overwrite_if_exists: bool = False,
        output_dir: str = ".",
        config_file: str = "",
    ) -> str:
        ...


class Cookiecutter(Template):
    def __init__(
        self,
        template_directory: Path,
        injected_cookiecutter: Optional[CookiecutterAPI] = None,
    ):
        """
        Wraps Cookiecutter with possibly a not templated folder name.

        This implementation allows for templates to not contain a templated
        folder name as the templated root.

        A cookiecutter template normally has the following form:

        .. code-block:: none

            template_name/
            │
            ├── {{ cookiecutter.project_slug }}/
            │   │
            │   └── <files and folders within the template>
            │
            └── cookiecutter.json

        *   A template root folder (shown as ``template_name``)

        *   The templated root folder (shown as ``{{ cookiecutter.project_slug }}``)

            Even if this exact name is not required, cookiecutter do requires
            this folder to have a templated name.

        *   Templated files and folder under the templated root folder

        *   Default values for the template in ``cookiecutter.json``

        This implementation also allows the following form:

        .. code-block:: none

            template_name/      # \\
            │                   #  - those two have the same name
            ├── template_name/  # /
            │   │
            │   └── <files and folders within the template>
            │
            └── cookiecutter.json

        by copying the template in a temporary folder, renaming the
        templated root folder ``{{ cookiecutter.project_slug }}`` and
        running this as the template.

        .. important::
            The template root folder and the templated root folder
            must have the same name!

            In the example above, both are named ``template_name``.

        In case the default values (from ``cookiecutter.json``) does not
        provide a ``project_slug`` value, the template root filename is
        used (``template_name`` above).

        Args:
            template_directory: A folder that contains a cookiecutter
                template. Cookiecutter will complait in case it cannot
                find a ``cookiecutter.json`` file and a templated
                folder in it.
            injected_cookiecutter: An alternative implementation of
                the cookiecutter API protocol.  This can be rather
                convenient to mock the implementation without having
                to use monkeypatching.
        """
        self._path = template_directory
        self._cookiecutter = injected_cookiecutter

    @property
    def _cookiecutter_api(self) -> CookiecutterAPI:
        if self._cookiecutter is None:
            from cookiecutter.main import cookiecutter  # type: ignore

            self._cookiecutter = cast(CookiecutterAPI, cookiecutter)
        return self._cookiecutter

    def _do_the_rendering(
        self, template_path: Path, output_folder: Path, data: Dict[str, str]
    ) -> Path:
        output_folder.mkdir(parents=True, exist_ok=True)

        data.setdefault("project_slug", self._path.name)

        result = self._cookiecutter_api(
            str(template_path),
            no_input=True,
            output_dir=str(output_folder),
            extra_context=data,
        )

        return Path(result)

    def render(
        self,
        parent_folder: Path,
        data: Optional[Dict[str, str]] = None,
    ) -> Path:
        data = data or {}
        template_name = self._path.name

        possibly_not_templated_foldername = self._path.joinpath(template_name)

        if possibly_not_templated_foldername.exists():
            with tempfile.TemporaryDirectory() as tmpdirname:
                tmp_template = Path(tmpdirname, template_name)
                shutil.copytree(self._path, tmp_template)

                template_folder = tmp_template.joinpath(template_name)
                templated_folder = template_folder.with_name(
                    "{{ cookiecutter.project_slug }}"
                )
                template_folder.rename(templated_folder)

                rendered = self._do_the_rendering(
                    tmp_template, parent_folder, data
                )
        else:
            rendered = self._do_the_rendering(self._path, parent_folder, data)
        return rendered

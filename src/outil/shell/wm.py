"""
#######################
Terminal window manager
#######################

A thin wrapper around a wrapper around a wrapper around Tmux.
"""

from pathlib import Path
from typing import Any, Dict, Optional

import libtmux.exc  # type: ignore
import libtmux.server  # type: ignore
import tmuxp.config  # type: ignore
import tmuxp.workspacebuilder  # type: ignore


class Tmux:
    def __init__(self, tmuxp_conf: Dict[str, Any]) -> None:
        self._tmuxp_conf = tmuxp_conf

    def start(self) -> None:
        builder = tmuxp.workspacebuilder.WorkspaceBuilder(
            tmuxp.config.trickle(tmuxp.config.expand(self._tmuxp_conf, ".")),
            server=libtmux.server.Server(),
        )
        try:
            builder.build()
        except libtmux.exc.TmuxSessionExists:
            builder.build(builder.session)
        builder.session.attach_session()
        for plugin in builder.plugins:
            plugin.before_script(builder.session)


class PreconfiguredTmux(Tmux):
    def __init__(
        self,
        path: Path,
        name: Optional[str] = None,
        editor_cmd: Optional[str] = None,
        repl_cmd: Optional[str] = None,
    ) -> None:
        name = name or path.name
        tmuxp_config = {
            "session_name": name,
            "start_directory": str(path),
            "shell_command_before": [],
            "windows": [
                {
                    "window_name": "mainwindow",
                    "focus": True,
                    # Maybe a bug here: https://github.com/tmux-python/tmuxp/issues/667
                    "layout": "3951,189x53,0,0[189x35,0,0,51,189x17,0,36{94x17,0,36,52,94x17,95,36,53}]",  # noqa: E501
                    "options": {"main-pane-height": 35},
                    "panes": [
                        {
                            "shell_command": editor_cmd or "outil editor",
                            "focus": True,
                        },
                        "pane",
                        {
                            "shell_command": repl_cmd or "outil repl",
                        },
                    ],
                }
            ],
        }
        super().__init__(tmuxp_config)

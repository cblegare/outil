"""
######################
Command line interface
######################

This module provides argument parsing, shell completion and other CLI
goodies.
"""

import logging
import os
from functools import partial
from pathlib import Path
from typing import Any, Dict, List, Sequence, Tuple

import click

from outil import __dist__, __project__, __version__
from outil.app.command import Command, CommandHandler
from outil.app.ioc import DependencyResolver, ServiceMapHandler
from outil.app.services import all_definitions
from outil.code.docstring import docstring_summary
from outil.template import Templates
from outil.workspace import Workspace

log = logging.getLogger(__project__)

DEFAULT_DOTFILES_DIR = os.path.join(click.get_app_dir(__project__), "dotfiles")


def help_from_object(obj: Any, default=""):
    return docstring_summary(obj) or default


class App:
    def __init__(
        self,
        handler: CommandHandler,
        templates: Templates,
        workspace: Workspace,
    ):
        self._handler = handler
        self._templates = templates
        self._workspace = workspace

    @property
    def templates(self) -> Templates:
        return self._templates

    def template_completion(
        self, ctx: click.Context, args: List[str], incomplete: str
    ) -> List[Tuple[str, str]]:
        suggestions = [
            # workaround bug httqps://github.com/pallets/click/issues/1812
            (
                name.replace(":", r"\:"),
                help_from_object(
                    self.templates[name], default="Undocumented template"
                ),
            )
            for name in self.templates
            if incomplete in name
        ]
        return suggestions

    def do(self, command: Command) -> Any:
        return command.do(self._handler)


def make_app() -> App:
    builder = DependencyResolver()

    for service_name, service_factory in all_definitions().items():
        builder.register(service_factory, name=service_name)

    handler = ServiceMapHandler(builder.build())
    app_with_bound_handler = partial(App, handler)
    app_with_bound_arguments = handler.inject(app_with_bound_handler)

    return app_with_bound_arguments()


_app = make_app()


@click.group(invoke_without_command=True)
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase log level. '-v' will output INFO messages. "
    "More than -vv is useless.",
)
@click.option(
    "-q",
    "--quiet",
    count=True,
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.pass_context
def app(ctx: click.Context, verbose: int, quiet: int) -> None:
    """
    outil, the One UTIL package to rule them all.
    """
    verbosity: int = int(logging.INFO / 10) + verbose - quiet
    logging_config = logging_configuration(verbosity)
    logging.basicConfig(**logging_config)

    ctx.obj = _app

    log.debug("Here are found dotfiles templates:")
    for template_name in ctx.obj.templates:
        log.debug(
            "  {!s} {!s}".format(
                template_name, ctx.obj.templates[template_name]
            )
        )

    if ctx.invoked_subcommand is None:
        click.echo("not really")


@app.command()
def labz() -> None:
    """Ignore this."""


@app.command()
@click.argument("things", nargs=-1)
def code(things: Sequence[str]) -> None:
    """
    New feature: python programming tooling

    Fluff idea: nacre, Trivia (gastropod)
    """
    from outil.code import do

    do(*things)


@app.command("ide")
@click.argument("project")
def development_environment(project: str) -> None:
    """
    Start various tools in managed windows like an IDE.
    """
    from outil.shell.wm import PreconfiguredTmux
    from outil.workspace.project import GitRepositories

    workspace = GitRepositories(Path.home().joinpath("git"), lambda x: x[0])
    project_path = workspace.find_project(project)
    ide = PreconfiguredTmux(project_path)
    ide.start()


@app.command()
def repl() -> None:
    """Start a Python read-eval-print-loop shell."""
    from outil.shell.repl import REPL

    REPL().start()


@app.command()
@click.argument("editor_args", nargs=-1, type=click.UNPROCESSED)
def editor(editor_args: List[str]) -> None:
    """Start my favorite editor."""
    from outil.shell.editor import Editor

    Editor(*editor_args).start()


@app.command()
@click.argument(
    "template",
    metavar="TEMPLATE",
    autocompletion=_app.template_completion,  # type: ignore
)
@click.option(
    "-o",
    "--output-dir",
    default=str(Path.cwd()),
    help=f"Where to render the files (default: {Path.cwd()})",
    metavar="PATH",
)
@click.option(
    "-d",
    "--data",
    help="Data to pass to the template as 'key=value'",
    multiple=True,
)
@click.pass_obj
def new(obj: App, template: str, output_dir: str, data: List[str]) -> None:
    """Render a templated folder."""
    from outil.template import New

    command = New(template, Path(output_dir), parse_cli_keyvalues(data))
    log.debug(f"Executing command {command!r}")
    click.echo(obj.do(command))


@app.command()
@click.argument(
    "template",
    metavar="TEMPLATE",
    autocompletion=_app.template_completion,  # type: ignore
    default="mydotfiles",
)
@click.option(
    "-o",
    "--dotfiles-dir",
    default=DEFAULT_DOTFILES_DIR,
    help=f"Where to render dotfiles (default: {DEFAULT_DOTFILES_DIR})",
    metavar="PATH",
)
@click.option(
    "-h",
    "--home",
    default=str(Path.home()),
    help=f"Home (default: {Path.home()})",
    metavar="PATH",
)
@click.option(
    "-d",
    "--data",
    help="Data to pass to the template as 'key=value'",
    multiple=True,
)
@click.pass_obj
def dotfiles(
    obj: App, template: str, dotfiles_dir: str, home: str, data: List[str]
) -> None:
    """
    Render dotfiles, then symlink them to your home folder.
    """
    from outil.template import Dotfiles

    command = Dotfiles(
        template, Path(dotfiles_dir), Path(home), parse_cli_keyvalues(data)
    )
    log.debug(f"Executing command {command!r}")
    click.echo(obj.do(command))


@app.group()
def report() -> None:
    """Build informational artifacts."""
    pass


@report.command()
def requirements() -> None:
    """Analyse and report Python project requirements."""
    # possible improvments:
    #   - parse at path
    #   - multiple definition schemes (pipenv, poetry, etc)
    #   - transitive (report whole dep tree)
    #   - extras
    for requirement in __dist__.requires():
        print(requirement)


def parse_cli_keyvalues(keyvalues: List[str]) -> Dict[str, str]:
    """
    Parse 'key=value' strings.

    Args:
        keyvalues: Strings to be split in 'key: value' pairs.

    Returns:
        A dictionary of parsed keys and values.
    """
    parsed = {
        k: v for k, v in map(lambda s: s.split("=", maxsplit=1), keyvalues)
    }
    return parsed


def logging_configuration(verbosity: int) -> Dict[str, Any]:
    """
    Build logging configuration based on a verbosity level.

    Args:
        verbosity: Verbosity 0 is means "CRITICAL", and each
            increments move toward "DEBUG".
    """
    level = logging.CRITICAL - logging.DEBUG * verbosity

    if level < logging.DEBUG:
        format = "%(levelname)8s %(name)-24s> %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(
        level=level,
        format=format,
    )


if __name__ == "__main__":
    app(prog_name=__project__)

"""
##########
User Input
##########

What gives?
"""
from typing import Optional, Sequence, TypeVar

from typing_extensions import Protocol

_T = TypeVar("_T")


class ChoicePrompt(Protocol[_T]):
    def __call__(self, choices: Sequence[_T]) -> Optional[_T]:
        ...

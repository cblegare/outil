import os

from pkg_resources import DistributionNotFound, get_distribution
from sphinx.application import Sphinx

__project__ = "outil_theme"
__version__ = None  # required for initial installation

try:
    distribution = get_distribution(__project__)
    __version__ = distribution.version

except DistributionNotFound:
    # This will happen if the package is not installed.
    # For more informations about development installation, read about
    # the 'develop' setup.py command or the '--editable' pip option.
    # Note that development installations may break other packages from
    # the same implicit namespace
    # (see https://github.com/pypa/packaging-problems/issues/12)
    __version__ = "(local)"
else:
    pass


def setup(app: Sphinx) -> None:
    app.add_html_theme(
        "outil_theme", os.path.abspath(os.path.dirname(__file__))
    )

==================
Interactive Shells
==================

Thierry likes having interactions with other crabs.  That's why he
always keeps a nice interactive shell at hand.

Thierry knows two things about interactive shells:

- how to pimp them (with scary anemones or the like).
- how to wear them (and strafe his way out of situations)

But Thierry is a digital pagurus and the classy calcareous exoskeleton
are useless to him.  Instead he really digs command line shells and
make them awesome with some dotfiles management.

And unlike his analog counterparts, Thierry can't simply scavenge 
abandonned shells from the seabed. Instead he uses *forks* to 
*spawn* something to wear.


.. todo::

    The following documentation do not comply with Thierry's style guide
    since it is not funny prose.


.. todo::

    Wait, what are you talking about with `Thierry's style guide`? Such a 
    thing don't even exists?!.


thierry
=======

.. code-block:: none

    thierry [OPTIONS] COMMAND [ARGS]...

.. program:: thierry

.. option:: --version

    Show the version and exit.

.. option:: --help

    Show a simple help message and exit.

    You can also use ``--help`` after any command to get help about this
    specific command.


thierry dotfiles
----------------

Render dotfiles, then symlink them to your home folder.

.. code-block:: none

    thierry dotfiles [OPTIONS]

.. program:: thierry dotfiles

.. option:: -t, --template <template_key>

    Dotfiles template key to apply 
    (default: ``thierry.dotfiles.templates:default``)

.. option:: -d, --dotfiles-dir <dotfiles_dir>

    Where to put dotfiles. By default, this is 
    ``<config_dir>/dotfiles`` where ``<config_dir>`` is given by
    :func:`click.get_app_dir`.

.. option:: -h, --home <home_dir>

    Mainly for testing purpose, this can be used to tell `thierry`
    to make symlink in a folder other than ``$HOME``.

.. option:: --non-interactive

    .. todo:: 
    
        :option:`--non-interactive` is not implemented yet.  
        Your help is needed!

    Since dotfiles are first rendered using Cookiecutter, interactive
    prompts can occur.  This flag disables prompts and forces the use
    of default values.

.. option:: --set <name>=<value>

    .. todo::
    
        :option:`--set` is not implemented yet.  
        Your help is needed!
    
    Inject a value to the Cookiecutter template before the render of
    dotfiles.




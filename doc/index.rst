#######
|outil|
#######

.. only:: html

    |build_badge| |cov_badge| |lic_badge| |home_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/outil/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/outil/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/outil/badges/master/coverage.svg
        :target: _static/embed/coverage/index.html
        :alt: Coverage Report

    .. |lic_badge| image:: https://img.shields.io/badge/license-BSD--2--Clause--Patent-brightgreen
        :target: https://spdx.org/licenses/BSD-2-Clause-Patent.html
        :alt: BSD-2-Clause Plus Patent License, see :ref:`license`.

    .. |home_badge| image:: https://img.shields.io/badge/gitlab-cblegare%2Foutil-orange.svg
        :target: https://gitlab.com/cblegare/outil
        :alt: Home Page


.. sidebar:: Thierry the hermit

    .. figure:: _static/thierry.png
        :align: center

        Thierry is a hermit crab who loves shells.
        Without one, Thierry feels naked and vulnerable.
        He has one for every occasion!


Opinionated utils for shell life-cycling.  |outil|'s mascot is Thierry.

outil is built with Python and friends and Thierry is a hermit crab who
loves shells.  Without one, Thierry feels naked and vulnerable.
He has one for every occasion!

In other words, |outil|

*   wraps Cookiecutter_ badly,

*   mimics RCM_ poorly,

*   tries to glue together tmux, nvim and ptpython to produce a dev tool,

*   contains my dotfiles (what!?) but also

*   is a somewhat decent example boilerplate of a Python project.

*   and is pretty much an over engineering dystopia.


Browse also the `project's home`_.


.. only:: latex

    The latest version of this document is also available online in the Docs_.

    .. _Docs: https://cblegare.gitlab.io/outil/


.. only:: html

    Read also the |pdfdownload|.


.. toctree::
     :maxdepth: 2

     dotfiles
     changelog
     license


======================
Technical Informations
======================

Here lies guides and references for contributors as well as some general
references.

.. toctree::
    :maxdepth: 2

    apidoc/index
    glossary
    release


==================
Tables and indices
==================

Quick access for not so much stuff.

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _project's home: https://gitlab.com/cblegare/outil/
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _RCM: http://thoughtbot.github.io/rcm/rcm.7.html
